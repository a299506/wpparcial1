# WPparcial1

# Descripción del proyecto
Sistema diseñado para la gestión de proyectos de desarollo de software, a través de este proyecto una empresa podrá gestionar sus proyectos mediante metodologia agil SCRUM. En esta primera fase de proyecto se han agregado las rutas que se utilizaran en base a los diagramas de clases y al modelo REST.


# Diagrama de clases
![Diagrama de clases](./resources/DiagramaDeClases/DiagramaDeClases.jpg)


# Diagrama de interacción
El diagrama de interaccion lo dividimos en varios para poder poder tener un mejor manejo de estos.

El diagrama de creación de usuario
![Diagrama de interacción de creación de usuario](./resources/DiagramasDeInteraccion/DiagramaInteraccion-registro.jpg)

El diagrama de inicio de sesión
![Diagrama de interacción de inicio de sesión](./resources/DiagramasDeInteraccion/DiagramaInteraccion-Inicio%20de%20sesión.jpg)

El diagrama de creación de proyecto
![Diagrama de creación de proyecto](./resources/DiagramasDeInteraccion/DiagramaInteraccion-Creación%20de%20proyecto.jpg)

El diagrama de uso de tablero de control
![Diagrama de uso de tablero de control](./resources/DiagramasDeInteraccion/DiagramaInteraccion-Uso%20de%20tablero%20de%20control.jpg)

Aqui lo dividimos en scrum también ya que al ser scrum tienes más privilegios
![Diagrama de uso de tabelro de control master](./resources/DiagramasDeInteraccion/DiagramaInteraccion-Uso%20de%20tablero%20de%20control%20master.jpg)

# Herramientas utilizadas
-Express con el modelo de vistas PUG
-Node.js

# Estructura del Proyecto
public/: Carpeta que contiene archivos estáticos como CSS, JavaScript e imágenes.

views/: Contiene los archivos Pug para las vistas, organizados por categoría.

routes/: Definición de las rutas del proyecto.

controllers/: Controladores para gestionar las acciones y la lógica en base al modelo REST.

app.js: Archivo principal de la aplicación que configura Express y las rutas.

package.json: Archivo de configuración de npm que lista las dependencias y scripts.

# Imagen de Docker
imagen en Dockerhub accesible desde:
        https://hub.docker.com/repository/docker/joseconder/proyectowp1/general

Para descargar el contenedor 
        docker pull joseconder/proyectowp1:newest

# Ejecución
-En la terminal, abrir la carpeta raiz del proyecto
-Ejecutar el comando "node start"

# Creditos

Jose Eduardo Conde Hernandez 299506

Miriam Fernanda Arellanes Perez 353256

Jesús Manuel Calleros Vázquez 348737

Proyecto realizado para la materia web platforms del Ing. Luis Antonio Ramirez